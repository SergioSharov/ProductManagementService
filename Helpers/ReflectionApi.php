<?php

namespace SergioSharov\ProductManagementService\Helpers;

class ReflectionApi
{
    public static function getReflectionClass($class)
    {
        return new \ReflectionClass($class);
    }

    public static function getPrivateProperties($class)
    {
        return self::getReflectionClass($class)->getProperties(\ReflectionProperty::IS_PRIVATE);
    }
}