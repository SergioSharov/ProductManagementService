<?php
namespace SergioSharov\ProductManagementService\Helpers;

class SQLHelper
{
    private $excludedProperties = [
        'id'
    ];

    private $includedMethodPrefix = [
        'is',
        'get'
    ];

    public function getTableNameFromClass($class)
    {
        $class = get_class($class);

        $docComment = ReflectionApi::getReflectionClass($class)->getDocComment();

        $tag = "@TableName('";

        $tableNameHaystack = substr($docComment, strpos($docComment, $tag)+strlen($tag));

        return substr($tableNameHaystack,0, strpos($tableNameHaystack, "')"));
    }

    public function getColumns($class, $withExcluded = false)
    {
        $properties = ReflectionApi::getPrivateProperties($class);

        $result = [
            'columns' => [],
            'values' => []
        ];
        foreach ($properties as $column) {
            $columnName = $column->getName();

            if ($withExcluded && in_array($columnName, $this->excludedProperties)) {
                continue;
            }

            foreach ($this->includedMethodPrefix as $includedMethodPrefix) {
                if (method_exists($class, sprintf('%s%s', $includedMethodPrefix, ucfirst($columnName)))) {
                    $value = $class->{sprintf('%s%s', $includedMethodPrefix, ucfirst($columnName))}();

                    if ($value) {
                        $result['columns'][] = $columnName;
                        $result['values'][] = $value;
                    }
                }
            }
        }

        return $result;
    }

    public function getParams($columns) {
        return preg_replace('/(\w+)/', '?', implode(',', $columns));
    }

    public function getTypes($columns) {
        $result = '';
        foreach ($columns as $column) {
            switch (gettype($column)) {
                case 'string':
                    $result .= 's';
                    break;
                case 'integer':
                case 'boolean':
                    $result .= 'i';
                    break;
                default:
                    $result .='s';
                    break;
            }
        }

        return $result;
    }


    public function checkParamsToColumns($class, $params)
    {
        $properties = ReflectionApi::getPrivateProperties($class);

        $columns = [];
        foreach ($properties as $column) {
            $columns[] = $column->getName();
        }

        $paramKeys = array_keys($params);

        return !count(array_diff($paramKeys, $columns));
    }

    public function getCriteriaFromParams($params) {
        $result = '';

        foreach ($params as $k => $param) {
            if ($result !== '') {
                $result .= ' AND';
            }

            $result .= sprintf(' %s = ?', $k);
        }

        return $result;
    }

}