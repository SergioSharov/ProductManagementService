<?php
require_once "vendor/autoload.php";

use SergioSharov\ProductManagementService\Controller\ProductController;

if ($_SERVER['CONTENT_TYPE'] === 'application/json'){
    $_POST = json_decode(file_get_contents('php://input'), true);
}

if ($_POST=== null){
    echo json_encode(['error'=>'Invalid JSON']);
    return http_response_code(400);
}

$command = $_SERVER['HTTP_COMMAND'];
$controller = new ProductController();

switch ($command) {
    case 'GET':
        echo $controller->actionGet();
        break;
    case 'LIST':
        echo $controller->actionList();
        break;
    case 'CREATE':
        echo $controller->actionCreate();
        break;
    case 'DELETE':
        echo $controller->actionDelete();
        break;
    case 'UPDATE':
        echo $controller->actionUpdate();
        break;
    default:
        echo 'Invalid command';
        break;
}

