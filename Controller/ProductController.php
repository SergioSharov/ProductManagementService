<?php

namespace SergioSharov\ProductManagementService\Controller;

use SergioSharov\ProductManagementService\Entity\Product;
use SergioSharov\ProductManagementService\Repository\MysqlConnection;
use SergioSharov\ProductManagementService\Repository\ProductRepository;

class ProductController extends Controller
{
    private $mysqlConnection;

    private $productRepository;

    public function __construct()
    {
        $this->mysqlConnection = MysqlConnection::getInstance('localhost', 'root', '', 'ProductBD');

        $this->productRepository = new ProductRepository($this->mysqlConnection);
    }

    public function actionList()
    {

        $products = $this->productRepository->select(new Product(), []);

        return $this->getResponse(200, $products);
    }

    /**
     * @param $form
     * @return mixed|string
     */
    public function actionCreate()
    {
        $product = new Product();

        if (!$this->checkProductData()) {
            return $this->getResponse(400, '');
        }

        $product->setTitle($_POST['title'])
                ->setQty($_POST['qty'])
                ->setPrice($_POST['price'])
                ->setCurrency($_POST['currency']);

        $this->productRepository->insert($product);

        return $this->getResponse(201, $product);
    }

    public function actionUpdate()
    {
        $id = $_POST['id'];

        $product = $this->productRepository->select(new Product(), ['id' => $id]);

        if (empty($product)) {
            return $this->getResponse(404, $product);
        }

        if (!$this->checkProductData()) {
            return $this->getResponse(400, '');
        }

        $product->setTitle($_POST['title'])
            ->setQty($_POST['qty'])
            ->setPrice($_POST['price'])
            ->setCurrency($_POST['currency']);

        $this->productRepository->update($product);

        return $this->getResponse(200, $product);
    }

    public function actionDelete()
    {
        $id = $_POST['id'];

        $product = $this->productRepository->select(new Product(), ['id' => $id]);

        if (empty($product)) {
            return $this->getResponse(404, $product);
        }

        $this->productRepository->delete($product);

        return $this->getResponse(200, $product);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionGet()
    {
        $id = $_POST['id'];


        $product = $this->productRepository->select(new Product(), ['id' => $id]);

        if (empty($product)) {
            return $this->getResponse(404, $product);
        }

        return $this->getResponse(200, $product);
    }

    private function checkProductData()
    {
        if (empty($_POST['title']) || empty($_POST['qty']) || empty($_POST['price']) || empty($_POST['currency'])){
            return false;
        } else
            return true;
    }

}


