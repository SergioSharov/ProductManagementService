<?php

namespace SergioSharov\ProductManagementService\Controller;

use SergioSharov\ProductManagementService\Service\DataTransformer;

class Controller
{
    protected function getResponse(int $code, $data)
    {
        http_response_code($code);
        header('Content-Type: application/json');

        $transformer = new DataTransformer();


        return json_encode($transformer->transform($data));
    }

}