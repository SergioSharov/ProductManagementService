<?php

namespace SergioSharov\ProductManagementService\Service;

use SergioSharov\ProductManagementService\Helpers\ReflectionApi;

class DataTransformer
{
    public function transform($object)
    {
        $result = [];
        if (is_array($object)) {
            foreach ($object as $item) {
                $result[] = $this->transformObject($item);
            }

        } elseif (is_object($object)) {
            $result = $this->transformObject($object);
        }
        if (empty($result)) {
            return [
                'error' => 'Request is invalid'
            ];
        }
        return [
            'data' => $result
        ];
    }

    private function transformObject($object)
    {
        $properties = ReflectionApi::getPrivateProperties($object);

        $transformedObject = new \stdClass();
        $attributes = [];

        foreach ($properties as $property) {
            $name = $property->getName();
            $methodName = sprintf('get%s', ucfirst($name));
            if (method_exists($object, $methodName)) {
                $transformedObject->$name = $object->{$methodName}();
                if ($name !== 'id') {
                    $attributes[$name] = $object->{$methodName}();
                }
            }
        }

        $data = [
            'type' => strtolower(substr(get_class($object), strrpos(get_class($object), '\\')+1)),
            'id' => $transformedObject->id,
            'attributes' => $attributes
        ];

        return $data;
    }

}