<?php

namespace SergioSharov\ProductManagementService\Repository;

interface DbConnectionInterface
{
    public function insert($object);

    public function update($object);

    public function delete($object);

    public function select($object, $params);

}