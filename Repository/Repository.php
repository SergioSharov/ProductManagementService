<?php

namespace SergioSharov\ProductManagementService\Repository;

use SergioSharov\ProductManagementService\Helpers\DataMapper;


class Repository
{
    private $mysqlConnection;

    public function __construct(DbConnectionInterface $mysqlConnection)
    {
        $this->mysqlConnection = $mysqlConnection;
    }

    public function insert($object)
    {
        return $this->mysqlConnection->insert($object);
    }

    public function update($object)
    {
        return $this->mysqlConnection->update($object);
    }

    public function delete($object)
    {
        return $this->mysqlConnection->delete($object);
    }

    public function select($object, $params)
    {
        $rows = $this->mysqlConnection->select($object, $params);
        $dataMapper = new DataMapper();
        $result = [];

        foreach ($rows as $row) {
            $result[] = $dataMapper->arrayToData(clone $object, $row);
        }

        return count($result) === 1 ? $result[0] : $result;
    }


}