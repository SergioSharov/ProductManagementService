<?php

namespace SergioSharov\ProductManagementService\Repository;

use SergioSharov\ProductManagementService\Helpers\SQLHelper;

class MysqlConnection implements DbConnectionInterface
{
    protected $connection;

    protected $sqlHelper;

    private static $instances = [];

    public function __construct($hostname, $user, $pass, $db)
    {
        $this->connection = new \mysqli($hostname, $user, $pass, $db);

        $this->sqlHelper = new SQLHelper();
    }

    public static function getInstance($hostname, $user, $pass, $db)
    {
        $className = static::class;
        if (!isset(self::$instances[$className])) {
            self::$instances[$className] = new static($hostname, $user, $pass, $db);
        }

        return self::$instances[$className];
    }


    public function insert($object)
    {
        $tableName = $this->sqlHelper->getTableNameFromClass($object);
        $columns = $this->sqlHelper->getColumns($object, true);
        $params = $this->sqlHelper->getParams($columns['columns']);

        $types = $this->sqlHelper->getTypes($columns['values']);

        $stmt = $this->connection->stmt_init();

        $sql = sprintf('INSERT INTO %s (%s) VALUES (%s)', $tableName, implode(',', $columns['columns']), $params);

        $stmt->prepare($sql);
        $stmt->bind_param($types,...$columns['values']);

        return $stmt->execute();
    }

    public function update($object)
    {
        $tableName = $this->sqlHelper->getTableNameFromClass($object);
        $columns = $this->sqlHelper->getColumns($object, true);

        $idToUpdate = $object->getId();

        $types = $this->sqlHelper->getTypes($columns['values']);

        $stmt = $this->connection->stmt_init();


        $sql = sprintf('UPDATE %s SET %s where id = %s', $tableName, implode( ' = ?,', $columns['columns']) . ' = ?',  $idToUpdate);

        $stmt->prepare($sql);
        $stmt->bind_param($types,...$columns['values']);

        return $stmt->execute();
    }

    public function delete($object)
    {
        $tableName = $this->sqlHelper->getTableNameFromClass($object);
        $idToDelete = $object->getId();

        $stmt = $this->connection->stmt_init();

        $sql = sprintf('DELETE FROM %s WHERE id = ?', $tableName);

        $stmt->prepare($sql);

        $stmt->bind_param('i',$idToDelete);


        return $stmt->execute();
    }

    public function select($object, $params)
    {
        $tableName = $this->sqlHelper->getTableNameFromClass($object);

        if (!$this->sqlHelper->checkParamsToColumns($object, $params)) {
            return [];
        }

        $criteria = $this->sqlHelper->getCriteriaFromParams($params) ;
        $types = $this->sqlHelper->getTypes($params);

        $stmt = $this->connection->stmt_init();


        $sql = sprintf('SELECT * FROM %s WHERE %s', $tableName, $criteria != '' ? $criteria : 1);

        $stmt->prepare($sql);
        if ($types !== '') {
            $stmt->bind_param($types,...array_values($params));
        }

        $stmt->execute();

        $stmtResult = $stmt->get_result();

        $result = [];

        while ($row = $stmtResult->fetch_assoc()) {
            $result[] = $row;
        }

        return $result;
    }


}